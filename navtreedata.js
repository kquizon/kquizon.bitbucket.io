/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Directory", "index.html#sec_directory", null ],
    [ "References", "index.html#sec_references", null ],
    [ "Homework 1: Calculating Change", "page_Homework1.html", [
      [ "Description", "page_Homework1.html#page_Homework1_Description", null ],
      [ "Source Code Access", "page_Homework1.html#page_Homework1_src", null ]
    ] ],
    [ "Lab 1: VendoTron", "page_Lab1.html", [
      [ "Description", "page_Lab1.html#page_Lab1_desc", null ],
      [ "Source Code Access", "page_Lab1.html#page_Lab1_src", null ]
    ] ],
    [ "Lab 2: Test Your Reflexes", "page_Lab2.html", [
      [ "Description", "page_Lab2.html#page_Lab2_desc", null ],
      [ "Source Code Access", "page_Lab2.html#page_Lab2_src", null ]
    ] ],
    [ "Lab 3: ADC Voltage Conversion and Serial Communication", "page_Lab3.html", [
      [ "Description", "page_Lab3.html#page_Lab3_desc", null ],
      [ "Source Code Access", "page_Lab3.html#page_Lab3_src", null ],
      [ "Results", "page_Lab3.html#page_Lab3_results", null ]
    ] ],
    [ "Lab 4: Hot or Not?", "page_Lab4.html", [
      [ "Description", "page_Lab4.html#page_Lab4_desc", null ],
      [ "Source Code Access", "page_Lab4.html#page_Lab4_src", null ],
      [ "Collaboration", "page_Lab4.html#page_Lab4_collab", null ],
      [ "Results", "page_Lab4.html#page_Lab4_results", null ]
    ] ],
    [ "Lab 5: State Space Model of Tilt Table System", "page_Lab5.html", [
      [ "Description", "page_Lab5.html#page_Lab5_Desc", null ],
      [ "Partnership", "page_Lab5.html#page_Lab5_Partner", null ],
      [ "Hand Calculations", "page_Lab5.html#page_Lab5_HC", null ]
    ] ],
    [ "Lab 6: Simulation or Reality?", "page_Lab6.html", [
      [ "Description", "page_Lab6.html#page_Lab6_Desc", null ],
      [ "Source Code Access", "page_Lab6.html#page_Lab6_src", null ],
      [ "Linearization", "page_Lab6.html#page_Lab6_Linear", null ],
      [ "MATLAB Method", "page_Lab6.html#page_Lab6_MATLABMet", null ],
      [ "Open Loop Case 1 Results", "page_Lab6.html#page_Lab6_Case1", null ],
      [ "Open Loop Case 2 Results", "page_Lab6.html#page_Lab6_Case2", null ],
      [ "Open Loop Case 3 Results", "page_Lab6.html#page_Lab6_Case3", null ],
      [ "Open Loop Case 4 Results", "page_Lab6.html#page_Lab6_Case4", null ],
      [ "Closed Loop Case Results", "page_Lab6.html#page_Lab6_ClosedCase", null ]
    ] ],
    [ "Lab 7: Resistive Touch Panel Driver", "page_Lab7.html", [
      [ "Description", "page_Lab7.html#page_Lab7_desc", null ],
      [ "Source Code Access", "page_Lab7.html#page_Lab7_src", null ],
      [ "Hardware Setup", "page_Lab7.html#page_Lab7_hardware", null ],
      [ "Time Response of Driver", "page_Lab7.html#page_Lab7_time", null ]
    ] ],
    [ "Lab 8: Term Project Part I (Motors and Encoders, oh my!)", "page_Lab8.html", [
      [ "Description", "page_Lab8.html#page_Lab8_desc", null ],
      [ "Source Code Access", "page_Lab8.html#page_Lab8_src", null ],
      [ "Documentation", "page_Lab8.html#page_Lab8_doc", null ]
    ] ],
    [ "Term Project Hardware", "page_TPH.html", [
      [ "Description", "page_TPH.html#page_TPH_Desc", null ],
      [ "Controller Mounts", "page_TPH.html#page_TPH_Controller", null ],
      [ "Actuators", "page_TPH.html#page_TPH_Motor", null ],
      [ "Sensor 1: Output Shaft Encoder", "page_TPH.html#page_TPH_Encoder", null ],
      [ "Sensor 2: BNO055 IMU", "page_TPH.html#page_TPH_IMU", null ],
      [ "Sensor 3: Touch Panel", "page_TPH.html#page_TPH_touch", null ],
      [ "Hardware Functionality", "page_TPH.html#page_TPH_function", null ]
    ] ],
    [ "Term Project Software", "page_TPS.html", [
      [ "Description", "page_TPS.html#page_TPS_desc", null ],
      [ "Source Code Access", "page_TPS.html#page_TPS_src", null ],
      [ "Drivers", "page_TPS.html#page_TPS_drivers", null ],
      [ "Task Queueing and Priority Organization", "page_TPS.html#page_TPS_fileshare", null ],
      [ "Fault Handling", "page_TPS.html#page_TPS_Fault", null ],
      [ "General Functionality", "page_TPS.html#page_TPS_Functionality", null ],
      [ "User Manual", "page_TPS.html#page_TPS_User", null ]
    ] ],
    [ "Term Project Results", "page_TPR.html", [
      [ "Description", "page_TPR.html#page_TPR_desc", null ],
      [ "Tuning the Table", "page_TPR.html#page_TPR_tuning", null ],
      [ "Balancing the Ball", "page_TPR.html#page_TPR_balancing", null ],
      [ "Partnership", "page_TPR.html#page_TPR_partner", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classtask__share_1_1Queue.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';