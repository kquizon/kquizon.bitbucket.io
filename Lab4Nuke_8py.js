var Lab4Nuke_8py =
[
    [ "adc", "Lab4Nuke_8py.html#ad4919d994982fda12efb771158fd9cfa", null ],
    [ "data", "Lab4Nuke_8py.html#ab9ca8d6de7b2bb77e7bfe570a59eeff6", null ],
    [ "filename", "Lab4Nuke_8py.html#ab2f2261b5674cc93cfc2bc30ce000970", null ],
    [ "header", "Lab4Nuke_8py.html#a6dc5109427ecf33cac961ee79c2ed975", null ],
    [ "hrs", "Lab4Nuke_8py.html#a51495598c6771d8058a31a4adb964543", null ],
    [ "MCP9808", "Lab4Nuke_8py.html#a1ada6e606bbfddc00ec84dfa5820421e", null ],
    [ "t0", "Lab4Nuke_8py.html#affa2a79cf5a9ae3418278733b7de50fe", null ],
    [ "t_current", "Lab4Nuke_8py.html#a3609860c54755e3a0e887bcee2451ef3", null ],
    [ "t_iter", "Lab4Nuke_8py.html#a8f979e2f8a5b920081fc437e7427105f", null ],
    [ "temp", "Lab4Nuke_8py.html#a9861388955054336c2972fddeb6ba15b", null ],
    [ "temp_A", "Lab4Nuke_8py.html#a83c542b00a5c1df322ba9af35d0528d8", null ],
    [ "ticks_0", "Lab4Nuke_8py.html#a4d16d06ab6db6b3b1eaf02f1933b86b5", null ],
    [ "time_ms_run", "Lab4Nuke_8py.html#a180fa8cfc682887ff74a693ab1e3297a", null ],
    [ "time_s", "Lab4Nuke_8py.html#afb2e1e2bb73c9af3e8503b5ed7deaa8e", null ],
    [ "total_time", "Lab4Nuke_8py.html#a39c2491dd7ac5226622fb54635b03ac2", null ]
];