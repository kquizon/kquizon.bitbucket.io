var files_dup =
[
    [ "bno055.py", "bno055_8py.html", "bno055_8py" ],
    [ "bno055_base.py", "bno055__base_8py.html", "bno055__base_8py" ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "HW1.py", "HW1_8py.html", "HW1_8py" ],
    [ "Lab1.py", "Lab1_8py.html", "Lab1_8py" ],
    [ "Lab2.py", "Lab2_8py.html", "Lab2_8py" ],
    [ "Lab3Nuke.py", "Lab3Nuke_8py.html", "Lab3Nuke_8py" ],
    [ "Lab3PCUI.py", "Lab3PCUI_8py.html", "Lab3PCUI_8py" ],
    [ "Lab4Nuke.py", "Lab4Nuke_8py.html", "Lab4Nuke_8py" ],
    [ "Lab4Plot.py", "Lab4Plot_8py.html", "Lab4Plot_8py" ],
    [ "Lab7test.py", "Lab7test_8py.html", "Lab7test_8py" ],
    [ "MCP9808.py", "MCP9808_8py.html", "MCP9808_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "projectFile.py", "projectFile_8py.html", "projectFile_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "touchdriver.py", "touchdriver_8py.html", [
      [ "touchdriver", "classtouchdriver_1_1touchdriver.html", "classtouchdriver_1_1touchdriver" ]
    ] ]
];