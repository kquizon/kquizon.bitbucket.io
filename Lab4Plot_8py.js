var Lab4Plot_8py =
[
    [ "line", "Lab4Plot_8py.html#a10aa5b01f8a9cb5a92ed21490b622f26", null ],
    [ "ref_data", "Lab4Plot_8py.html#a039139d6857a2fc9ba2820a51cbe34a6", null ],
    [ "t", "Lab4Plot_8py.html#a2654e85143bb25c9c703a7630f029281", null ],
    [ "T_a", "Lab4Plot_8py.html#a92ba2d738ea36060c4e75b884278c8a3", null ],
    [ "T_c", "Lab4Plot_8py.html#af479b6b7138b64ab0f814f9033e67887", null ],
    [ "Ta", "Lab4Plot_8py.html#ae0dae4c0ce1376092aa06baac6268544", null ],
    [ "Ta_plot", "Lab4Plot_8py.html#ace43682af6d3fa50903da6922f262e53", null ],
    [ "Tc", "Lab4Plot_8py.html#a9a8f0ce7366457d8d72638f1f4abfef1", null ],
    [ "Tc_plot", "Lab4Plot_8py.html#a5d11c3efa7f3979f7a803578f6845c4c", null ],
    [ "time", "Lab4Plot_8py.html#a8b736c18c513fdd0e52ffe67a33723d4", null ],
    [ "time_plot", "Lab4Plot_8py.html#ab8e822a46941eb582776c80ff5fbf3a6", null ]
];