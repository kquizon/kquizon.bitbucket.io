var hierarchy =
[
    [ "bno055_base.BNO055_BASE", "classbno055__base_1_1BNO055__BASE.html", [
      [ "bno055.BNO055", "classbno055_1_1BNO055.html", null ]
    ] ],
    [ "Encoder.Encoder", "classEncoder_1_1Encoder.html", null ],
    [ "MCP9808.MCP9808", "classMCP9808_1_1MCP9808.html", null ],
    [ "MotorDriver.MotorDriver", "classMotorDriver_1_1MotorDriver.html", null ],
    [ "task_share.Queue", "classtask__share_1_1Queue.html", null ],
    [ "task_share.Share", "classtask__share_1_1Share.html", null ],
    [ "cotask.Task", "classcotask_1_1Task.html", null ],
    [ "cotask.TaskList", "classcotask_1_1TaskList.html", null ],
    [ "touchdriver.touchdriver", "classtouchdriver_1_1touchdriver.html", null ],
    [ "Lab7test.touchtest", "classLab7test_1_1touchtest.html", null ]
];